<?php

// http://code.google.com/apis/base/docs/2.0/query-lang-spec.html
// http://code.google.com/apis/base/docs/2.0/ranking-lang-spec.html

/**
 * Implementation of hook_views_plugins().
 */
function googlebase_views_plugins() {
  return array(
    'module' => 'googlebase',
    'query' => array(
      'googlebase_views_query' => array(
        'title' => t('Google Base Query'),
        'help' => t('Query that allows you to search Google Base.'),
        'handler' => 'googlebase_views_query',
        'parent' => 'views_query',
        'path' => drupal_get_path('module', 'googlebase') . '/views/plugins',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function googlebase_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'googlebase') . '/views/handlers',
    ),
    'handlers' => array(
      'googlebase_views_handler_field_generic' => array(
        'parent' => 'views_handler_field',
      ),
      'googlebase_views_handler_field_datetime' => array(
        'parent' => 'views_handler_field_date',
      ),
      'googlebase_views_handler_filter_search' => array (
         'parent' => 'views_handler_filter_search',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function googlebase_views_data() {
  
  $data['googlebase']['table']['group']  = t('Google Base');

  $data['googlebase']['table']['base'] = array(
    'query class' => 'googlebase_views_query',
    'title' => t('Google Base'),
    'help' => t('Query items submitted to Google Base.'),
    'field' => 'nid',
  );

  $data['googlebase']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('The node ID of the node.'),
    'field' => array(
      'name field' => 'title',
      'numeric' => TRUE,
      'handler' => 'googlebase_views_handler_field_generic',
      'click sortable' => TRUE,
      'googlebase base handler' => array(
        'table' => 'node',
        'field' => 'nid',
      ),
    ),
  );
  $data['googlebase']['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of the node.'),
    'field' => array(
      'handler' => 'googlebase_views_handler_field_generic',
      'click sortable' => TRUE,
      'googlebase base handler' => array(
        'table' => 'node',
        'field' => 'title',
      ),
     ),
  );
  $data['googlebase']['published'] = array(
    'title' => t('Published'),
    'help' => t('The date the item was published.'),
    'field' => array(
      'handler' => 'googlebase_views_handler_field_datetime',
      'click sortable' => TRUE,
      'googlebase base handler' => array(
        'table' => 'node',
        'field' => 'published',
      ),
     ),
  );
  $data['googlebase']['content'] = array(
    'title' => t('Description'),
    'help' => t('A description of the item.'),
    'field' => array(
      'handler' => 'googlebase_views_handler_field_generic',
      'click sortable' => FALSE,
      'googlebase base handler' => array(
        'table' => 'node',
        'field' => 'content',
      ),
     ),
  );
  $data['googlebase']['updated'] = array(
    'title' => t('Updated'),
    'help' => t('Date and time of the modification done on this item.'),
    'field' => array(
      'handler' => 'googlebase_views_handler_field_datetime',
      'click sortable' => TRUE,
      'googlebase base handler' => array(
        'table' => 'node',
        'field' => 'updated',
      ),
     ),
  );
  // search filter
  $data['googlebase']['keys'] = array(
    'title' => t('Search Terms'), // The item it appears as on the UI,
    'help' => t('The terms to search for.'), // The help that appears on the UI,
    // Information for searching terms using the full search syntax
    'filter' => array(
      'handler' => 'googlebase_views_handler_filter_search',
    ),
  );
  

  return $data;
}
