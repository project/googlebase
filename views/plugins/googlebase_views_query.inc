<?php

/**
 * Class for handling a view that gets its data not from the database, but from
 * Google Base.
 */
class googlebase_views_query extends views_plugin_query {

  protected $query_string = array();
  protected $items_feed_url = GOOGLEBASE_ITEMS_FEED;

  /**
   * Constructor; Create the basic query object and fill with default values.
   */
  function init($base_table = 'node', $base_field = 'nid') {
    $this->base_table = $base_table;  // Predefine these above, for clarity.
    $this->base_field = $base_field;
    $this->relationships[$base_table] = array(
      'link' => NULL,
      'table' => $base_table,
      'alias' => $base_table,
      'base' => $base_table
    );

    // init the table queue with our primary table.
    $this->table_queue[$base_table] = array(
      'alias' => $base_table,
      'table' => $base_table,
      'relationship' => $base_table,
      'join' => NULL,
    );

    // init the tables with our primary table
    $this->tables[$base_table][$base_table] = array(
      'count' => 1,
      'alias' => $base_table,
    );

    if ($base_field) {
      $this->fields[$base_field] = array(
        'table' => $base_table,
        'field' => $base_field,
        'alias' => $base_field,
      );
    }

    $this->count_field = array(
      'table' => $base_table,
      'field' => $base_field,
      'alias' => $base_field,
      'count' => TRUE,
    );
  }

  /**
   * Add a field to the query table, possibly with an alias. This will
   * automatically call ensure_table to make sure the required table
   * exists, *unless* $table is unset.
   *
   * @param $table
   *   The table this field is attached to. If NULL, it is assumed this will
   *   be a formula; otherwise, ensure_table is used to make sure the
   *   table exists.
   * @param $field
   *   The name of the field to add. This may be a real field or a formula.
   * @param $alias
   *   The alias to create. If not specified, the alias will be $table_$field
   *   unless $table is NULL. When adding formulae, it is recommended that an
   *   alias be used.
   *
   * @return $name
   *   The name that this field can be referred to as. Usually this is the alias.
   */
  function add_field($table, $field, $alias = '', $params = NULL) {
    // We check for this specifically because it gets a special alias.
    if ($table == $this->base_table && $field == $this->base_field && empty($alias)) {
      $alias = $this->base_field;
    }

    if ($table && empty($this->table_queue[$table])) {
      //$this->ensure_table($table);
    }

    if (!$alias && $table) {
      $alias = $table . '_' . $field;
    }

    $name = $alias ? $alias : $field;

    // @todo FIXME -- $alias, then $name is inconsistent
    if (empty($this->fields[$alias])) {
      $this->fields[$name] = array(
        'field' => $field,
        'table' => $table,
        'alias' => $alias,
      );
    }

    foreach ((array)$params as $key => $value) {
      $this->fields[$name][$key] = $value;
    }

    return $name;
  }
  
  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $view->build_info['query'] = $this->query();
    $view->build_info['count_query'] = $this->query(TRUE);
    $view->build_info['query_args'] = array();
  }

  /**
   * Generate a query and a countquery from all of the information supplied
   * to the object.
   *
   * @param $get_count
   *   Provide a countquery if this is true, otherwise provide a normal query.
   */
  function query($get_count = FALSE) {

    $this->items_feed_url = GOOGLEBASE_ITEMS_FEED;

    $fields_array = $this->fields;
    if (!empty($fields_array)) {
      foreach ($fields_array as $field) {

      }
    }

    if (!empty($this->items_feed_url)) {
      $items = array();
      foreach ($this->query_string as $name => $value) {
        $items[] = $name . '=' . $value;
      }
      $this->items_feed_url .= '?' . implode('&', $items);
    }

    return $this->items_feed_url;
  }
  
  /**
   * Let modules modify the query just prior to finalizing it.
   */
  public function alter(&$view) {

  }

  public function add_clause($name, $value) {
    $this->query_string[$name] = $value;
  }
  
  /**
   * Executes the query and fills the associated view object with according
   * values.
   * 
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->pager['current_page'].
   */
  public function execute(&$view) {
    $start_time = views_microtime();

//    $view->result = array();
//    $this->_view_arguments = $view->argument;
//    $this->_view_filters = $view->filter;
//    $this->_base_path = $view->display[$view->current_display]->display_options['path'];
//    $this->_current_display = $view->current_display;
//    $this->_current_views_name = $view->name;

    $headers = googlebase_headers();
    $result = drupal_http_request($this->items_feed_url, $headers);

    $xml = new SimpleXMLElement($result->data);

    $view->total_rows = count($xml->entry);
    $view->result = array();
    foreach ($xml->entry as $item) {
      $view->result[] = $item;
    }
    $view->pager['current_page'] = 1;
    
    $view->execute_time = views_microtime() - $start_time;
  }
  
}