<?php


/**
 * Class for retrieving and rendering an arbitrary field from Google Base query.
 */
class googlebase_views_handler_field_datetime extends views_handler_field_date {
  
  /**
   * We don't need to ensure any tables.
   * So overwrite this method because it might be called by inherited methods.
   */
  public function ensure_my_table() {}

  function render($values) {
    $value = $values->{$this->field_alias};
    list($date, $time) = explode('T', substr($value, 0, -5));
    $values->{$this->field_alias} = strtotime($date . ' ' . $time);
    return parent::render($values);
  }

  /**
   * Tell the query object to retrieve this field.
   */
  public function query() {
    // Add the field.
    $this->field_alias = $this->query->add_field($this->table_alias, $this->real_field);
  }
  
  
}
