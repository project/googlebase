<?php

/**
 * Field handler to provide simple renderer that allows linking to a node.
 */
class googlebase_views_handler_filter_search extends views_handler_filter_search {
  var $no_single = TRUE;
  function option_definition() {
    $options = parent::option_definition();

    $options['operator']['default'] = 'optional';

    return $options;
  }

  /**
   * Provide simple equality operator
   */
  function operator_form(&$form, &$form_state) {
    $form['operator'] = array(
      '#type' => 'radios',
      '#title' => t('On empty input'),
      '#default_value' => $this->operator,
      '#options' => array(
        'optional' => t('Show All'),
        'required' => t('Show None'),
      ),
    );
  }

  /**
   * Provide a simple textfield for equality
   */
  function exposed_form(&$form, &$form_state) {
    if (isset($this->options['expose']['identifier'])) {
      $key = $this->options['expose']['identifier'];
      $form[$key] = array(
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $this->value,
        '#attributes' => array('title' => t('Enter the terms you wish to search for.')),
      );
    }
  }

  /**
   * Validate the options form.
   */
  function exposed_validate($form, &$form_state) {
    if (!isset($this->options['expose']['identifier'])) {
      return;
    }

    $key = $this->options['expose']['identifier'];
    if (!empty($form_state['values'][$key])) {
      $this->search_query = search_parse_query($form_state['values'][$key]);

      if ($this->search_query[2] == '') {
        form_set_error($key, t('You must include at least one positive keyword with @count characters or more.', array('@count' => variable_get('minimum_word_size', 3))));
      }
      if ($this->search_query[6]) {
        if ($this->search_query[6] == 'or') {
          drupal_set_message(t('Search for either of the two terms with uppercase <strong>OR</strong>. For example, <strong>cats OR dogs</strong>.'));
        }
      }
    }
  }

  /**
   * Add this filter to the query.
   *
   * Due to the nature of fapi, the value and the operator have an unintended
   * level of indirection. You will find them in $this->operator
   * and $this->value respectively.
   */
  function query() {
    if (!empty($this->value)) {
      $this->query->add_clause('bq', $this->value);
    }
  }
}
