<?php


/**
 * Class for retrieving and rendering an arbitrary field from Google Base query.
 */
class googlebase_views_handler_field_generic extends views_handler_field {
  
  
  /**
   * We don't need to ensure any tables.
   * So overwrite this method because it might be called by inherited methods.
   */
  public function ensure_my_table() {}
  
  /**
   * Tell the query object to retrieve this field.
   */
  public function query() {
    // Add the field.
    $this->field_alias = $this->query->add_field($this->table_alias, $this->real_field);
  }
  
  /**
   * Add additionally required fields.
   */
  public function add_additional_fields($fields = NULL) {
//    if (!empty($fields)) {
//      return $this->_original->add_additional_fields($fields);
//    }
//    foreach ($this->_original->additional_fields as $f) {
//      $this->_original->query->add_field($f);
//    }
  }
  
  /**
   * Called when click-sorting.
   */
  public function click_sort($order) {
    /* These fields have a special "*_sort" field for sorting: */
    $special_sort_fields = array(
      'name' => 'name_sort',
      'title' => 'title_sort',
    );
    
    if (empty($special_sort_fields[$this->real_field])) {
      $this->_original->query->add_sort(
          $this->_original->real_field, $order, TRUE);
    }
    else {
      $this->_original->query->add_sort(
          $special_sort_fields[$this->_original->real_field], $order, TRUE);
    }
  }
  
}
