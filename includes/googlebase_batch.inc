<?php

class googlebase_batch extends googlebase_item {
  // http://code.google.com/apis/gdata/batch.html#Submit_HTTP
  public $xml = NULL;
  protected $namespaces = array(
    'g' => 'http://base.google.com/ns/1.0',
    'app' => 'http://purl.org/atom/app#',
    'batch' => 'http://schemas.google.com/gdata/batch',
  );
  protected $base_element = 'feed';

  protected $insert = array();
  protected $update = array();
  protected $delete = array();

  public function insert($node) {
    $entry = $this->xml->addChild('entry');
    $item = new googlebase_item($entry, $this->namespaces);
    // <batch:id>itemA</batch:id>
    // <batch:operation type="insert"/>
    $item->add_namespaced_element('batch', 'id', $node->nid);
    $item->add_namespaced_element('batch', 'operation', NULL, array('type' => 'insert'));
    googlebase_build_entry($node, $item);
    $this->insert[$node->nid] = $node;
  }

  public function update($node) {
    $entry = $this->xml->addChild('entry');
    $item = new googlebase_item($entry, $this->namespaces);
    $item->add_element('id', GOOGLEBASE_ITEMS_FEED . '/' . $node->googlebase_id);
    $item->add_namespaced_element('batch', 'operation', NULL, array('type' => 'update'));
    googlebase_build_entry($node, $item);
    $this->update[$node->googlebase_id] = $node;
  }

  public function delete($node) {
    $entry = $this->xml->addChild('entry');
    $item = new googlebase_item($entry, $this->namespaces);
    $item->add_element('id', GOOGLEBASE_ITEMS_FEED . '/' . $node->googlebase_id);
    $item->add_namespaced_element('batch', 'operation', NULL, array('type' => 'delete'));
    $this->delete[$node->googlebase_id] = $node->nid;
  }

  public function execute() {
    $headers = googlebase_headers();
    $url = GOOGLEBASE_BATCH_FEED;
    $method = 'POST';

    // Execute request
    $result = drupal_http_request($url, $headers, $method, $this->get_xml());
    $xml = new SimpleXMLElement($result->data);
    watchdog('googlebase', t('Response from Google Base: ') . print_r($result, TRUE));

/*    $data = '<?xml version="1.0" encoding="UTF-8"?>
<atom:feed xmlns:atom="http://www.w3.org/2005/Atom" xmlns:openSearch="http://a9.com/-/spec/opensearchrss/1.0/" xmlns:gm="http://base.google.com/ns-metadata/1.0" xmlns:g="http://base.google.com/ns/1.0" xmlns:batch="http://schemas.google.com/gdata/batch"><atom:id>http://www.google.com/base/feeds/items/batch/1255410666472</atom:id><atom:updated>2009-10-13T05:11:08.921Z</atom:updated><atom:title type="text">Batch Feed</atom:title><atom:entry><atom:id>http://www.google.com/base/feeds/items/3794609975146776451</atom:id><atom:published>2009-10-13T05:11:08.000Z</atom:published><atom:updated>2009-10-13T05:11:08.000Z</atom:updated><app:control xmlns:app="http://purl.org/atom/app#"><app:draft>yes</app:draft></app:control><atom:category scheme="http://base.google.com/categories/itemtypes" term="Poll"/><atom:title type="text">Does this work?</atom:title><atom:content type="html">* Yes
* No</atom:content><atom:link rel="alternate" type="text/html" href="http://base.google.com/base/a/5680316/D3794609975146776451"/><atom:link rel="self" type="application/atom+xml" href="http://www.google.com/base/feeds/items/3794609975146776451"/><atom:link rel="edit" type="application/atom+xml" href="http://www.google.com/base/feeds/items/3794609975146776451"/><atom:author><atom:name>deviable.com</atom:name><atom:email>anon-j3mld19e3j7q@base.google.com</atom:email></atom:author><g:target_country type="text">US</g:target_country><g:expiration_date type="dateTime">2038-01-19T03:14:07Z</g:expiration_date><g:customer_id type="int">5680316</g:customer_id><g:item_language type="text">en</g:item_language><g:item_type type="text">Poll</g:item_type><gd:feedLink xmlns:gd="http://schemas.google.com/g/2005" rel="media" href="http://www.google.com/base/feeds/items/3794609975146776451/media" countHint="0"/><batch:status code="201" reason="Created"/><batch:id>itemA</batch:id><batch:operation type="insert"/></atom:entry><atom:entry><atom:id>http://www.google.com/base/feeds/items/3976141961282869402</atom:id><atom:updated>2009-10-13T05:11:09.110Z</atom:updated><atom:title type="text">Deleted</atom:title><atom:content type="text">Deleted</atom:content><batch:status code="200" reason="Success"/><batch:operation type="delete"/></atom:entry><atom:entry><atom:id>http://www.google.com/base/feeds/items/2081450113330199833</atom:id><atom:updated>2009-10-13T05:11:09.110Z</atom:updated><atom:title type="text">Deleted</atom:title><atom:content type="text">Deleted</atom:content><batch:status code="200" reason="Success"/><batch:operation type="delete"/></atom:entry><atom:entry><atom:id>http://www.google.com/base/feeds/items/11610425975233874814</atom:id><atom:updated>2009-10-13T05:11:09.111Z</atom:updated><atom:title type="text">Deleted</atom:title><atom:content type="text">Deleted</atom:content><batch:status code="200" reason="Success"/><batch:operation type="delete"/></atom:entry></atom:feed>';
    $xml = new SimpleXMLElement($data); */

    $namespaces = $xml->getNamespaces(TRUE);
 
    foreach ($xml->xpath('/atom:feed/atom:entry') as $entry) {
      $batch = array();
      foreach($entry->children($namespaces['atom']) as $child) {
        $value = (string) $child;
        $name = $child->getName();
        if ($name == 'id') {
          $googlebase_id = substr(strrchr($value, '/'), 1);
          break;
        }
      }
      foreach($entry->children($namespaces['batch']) as $child) {
        $name = $child->getName();
        foreach($child->attributes() as $attribute => $value) {
          $batch[$name][$attribute] = (string) $value;
        }
        $batch[$name]['value'] = (string) $child;
      }

      if ($batch['operation']['type'] == 'insert') {
        if ($batch['status']['code'] == '201') {
          googlebase_save($this->insert[$batch['id']['value']], $googlebase_id);
        }
        elseif (!empty($batch['status']['code'])) {
          $this->log_error($batch['status']['code'], $batch['status']['reason'], $batch['operation']['type'], $batch['status']['value'], $this->insert[$batch['id']['value']]);
        }
        
      }
      elseif ($batch['operation']['type'] == 'update') {
        if ($batch['status']['code'] == '200') {
          googlebase_save($this->update[$googlebase_id], $googlebase_id);
        }
        elseif (!empty($batch['status']['code'])) {
          $this->log_error($batch['status']['code'], $batch['status']['reason'], $batch['operation']['type'], $batch['status']['value'], $this->update[$googlebase_id]);
          if ($batch['status']['code'] == '404') {
            googlebase_delete($this->update[$googlebase_id]);
          }
        }
      }
      elseif ($batch['operation']['type'] == 'delete') {
        if ($batch['status']['code'] == '200' || $batch['status']['code'] == '404') {
          googlebase_delete($this->delete[$googlebase_id]);
        }
        elseif (!empty($batch['status']['code'])) {
          $this->log_error($batch['status']['code'], $batch['status']['reason'], $batch['operation']['type'], $batch['status']['value'], node_load($this->delete[$googlebase_id]));
        }
      }
    }
  }

  protected function log_error($code, $reason, $op, $errors, $node) {
    try {
      $xml = new SimpleXMLElement($errors);

      if (!empty($node->nid)) {
        $messages[] = t('Item:') . ' ' . l($node->title, 'node/' . $node->nid);
      }
      $messages[] = t('Operation: !op', array('!op' => $op));
      foreach ($xml->error as $error) {
        $field = (string) $error['field'];
        if (!empty($field)) {
          $messages[] = t("Field %field: @reason\n", array('%field' => $field, '@reason' => (string) $error['reason']));
        }
        else {
          $messages[] = t("@reason\n", array('@reason' => (string) $error['reason']));
        }
      }
      $message = t("Error !code: !reason", array('!code' => $code, '!reason' => $reason)) . theme_item_list($messages);
      watchdog('googlebase', $message, NULL, WATCHDOG_ERROR);
    } catch (Exception $e) {
      watchdog('googlebase', t('There was an error communicating with Google Base.'));
    }
  }
}
