<?php

/**
 * @file googlebase_xml.inc
 *
 */

abstract class googlebase_xml {
  public $xml = NULL;
  protected $namespaces = array(
    'g' => 'http://base.google.com/ns/1.0',
  );

  protected $base_element = 'entry';

  function __construct(SimpleXMLElement &$xml = NULL, $namespaces = NULL) {
    if ($xml == NULL) {
      $xmltext = $this->generate();
      $this->xml = simplexml_load_string($xmltext);
    }
    else {
      $this->xml = $xml;
      if (!empty($namespaces)) {
        $this->namespaces = array_merge($this->namespaces, $namespaces);
      }
    }
  }

  protected function generate() {
    $base = $this->base_element;
    $xmltext = "<?xml version='1.0'?>\n";
    $xmltext .= "<$base xmlns='http://www.w3.org/2005/Atom'";

    foreach ($this->namespaces as $prefix => $namespace) {
      $xmltext .= " xmlns:{$prefix}='{$namespace}'";
    }

    $xmltext .= "></$base>";
    return $xmltext;
  }

  public function add_element($name, $value = NULL, $attributes = array()) {
    $node = $this->xml->addChild($name, $value);
    foreach ($attributes as $name => $value) {
      $node->addAttribute($name, $value);
    }
    return $node;
  }

  public function add_namespaced_element($namespace, $name, $value = NULL, $attributes = array()) {
    if (empty($this->namespaces[$namespace])) {
      return FALSE;
    }
    
    $node = $this->xml->addChild($namespace . ':' . $name, $value, $this->namespaces[$namespace]);
    foreach ($attributes as $name => $value) {
      $node->addAttribute($name, $value);
    }
    return $node;
  }

  /*
   * Adds a Google Base attribute (g: namespace);
   */
  public function add_attribute($name, $value, $type = NULL) {
    $node = $this->xml->addChild('g:' . $name, $value, $this->namespaces['g']);
    if (!empty($type)) {
      $node->addAttribute('type', $type);
    }
    return $node;
  }
  
  public function __toString() {
    return $this->xml->asXML();
  }

  public function get_xml() {
    return $this->xml->asXML();
  }
}