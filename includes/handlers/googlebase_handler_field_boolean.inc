<?php

/**
 * @file googlebase_handler_field_boolean.inc
 *
 */

/**
 * Description of googlebase_index_handler
 */
class googlebase_handler_field_boolean extends googlebase_handler_field {

  /**
   * Specifies which Google Base attribute type will be used for this field.
   *
   * @see http://code.google.com/apis/base/docs/2.0/reference.html#AttributesFeed
   *
   * @return string
   *   Possible values are: 'text', 'int', 'float', 'number', 'intUnit',
   *   'floatUnit', 'numberUnit', 'boolean', 'date', 'dateTime', 'dateTimeRange',
   *   'location', 'url', 'reference'.
   */
  public function get_type() {
    return 'boolean';
  }

  /**
   * Specifies the values that should be sent to Google Base.
   *
   * @return array
   *   An array of values to be sent to Google Base. The values returned should
   *   be based on the values stored in the field which are available in
   *   $this->field_values.
   */
  public function index_values() {
    $values = array();
    $keys = array_keys(optionwidgets_options($this->field_definition));
    $on_value = (!empty($keys) && isset($keys[1])) ? $keys[1] : NULL;
    foreach ($this->field_values as $value) {
      $values[] = $value['value'] == $on_value ? 'y' : 'n';
    }
    return $values;
  }
  
}
