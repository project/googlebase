<?php

/**
 * @file googlebase_handler_field_text.inc
 *
 */

/**
 * Maps CCK text fields to Google Base text attributes.
 */
class googlebase_handler_field_text extends googlebase_handler_field {

/**
 * Specifies which Google Base attribute type will be used for this field.
 *
 * @see http://code.google.com/apis/base/docs/2.0/reference.html#AttributesFeed
 *
 * @return string
 *   Possible values are: 'text', 'int', 'float', 'number', 'intUnit',
 *   'floatUnit', 'numberUnit', 'boolean', 'date', 'dateTime', 'dateTimeRange',
 *   'location', 'url', 'reference'.
 */
  public function get_type() {
    return 'text';
  }

  public function index_values() {
    $values = array();
    foreach ($this->field_values as $value) {
      $values[] = $value['value'];
    }
    return $values;
  }
  
}
