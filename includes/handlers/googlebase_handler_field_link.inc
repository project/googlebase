<?php

/**
 * @file googlebase_handler_field_link.inc
 *
 */

/**
 * Maps CCK links fields to Google Base url attributes.
 */
class googlebase_handler_field_link extends googlebase_handler_field {

  /**
   * Specifies which Google Base attribute type will be used for this field.
   *
   * @see http://code.google.com/apis/base/docs/2.0/reference.html#AttributesFeed
   *
   * @return string
   *   Possible values are: 'text', 'int', 'float', 'number', 'intUnit',
   *   'floatUnit', 'numberUnit', 'boolean', 'date', 'dateTime', 'dateTimeRange',
   *   'location', 'url', 'reference'.
   */
  public function get_type() {
    // 'link' is reserved for a link back to the node.
    if ($this->name == 'link') {
      return NULL;
    }
    return 'url';
  }

  /**
   * Specifies the values that should be sent to Google Base.
   *
   * @return array
   *   An array of values to be sent to Google Base. The values returned should
   *   be based on the values stored in the field which are available in
   *   $this->field_values.
   */
  public function index_values() {
    // 'link' is reserved for a link back to the node.
    if ($this->name == 'link') {
      return NULL;
    }
    
    $values = array();
    foreach ($this->field_values as $value) {
      $values[] = $value['value'] . $value['url'];
    }
    return $values;
  }
  
}
