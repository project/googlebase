<?php

/**
 * @file googlebase_handler_field.inc
 *
 */

/**
 * Base class for field handlers used to maps CCK fields to Google Base attributes.
 */
abstract class googlebase_handler_field {

  public $type = NULL;
  public $name = NULL;
  public $values = array();

  public $field_definition = NULL;
  public $field_values = NULL;
    
  function __construct($field, $field_values) {
    $this->field_definition = $field;
    $this->field_values = $field_values;
    $this->name = $this->get_name();
    $this->type = $this->validate_type($this->get_type());
    $this->values = $this->index_values();
  }

  abstract public function get_type();

  abstract public function index_values();

  public function get_name() {
    return substr($this->field_definition['field_name'], 6);
  }

  public function validate_type($type) {
    $valid_types = array(
      'text', 'int', 'float', 'number', 'intUnit', 'floatUnit', 'numberUnit',
      'boolean', 'date', 'dateTime', 'dateTimeRange', 'location', 'url',
      'reference',
    );

    return in_array($type, $valid_types) ? $type : FALSE;

  }

}


