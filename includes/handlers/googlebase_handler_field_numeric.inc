<?php

/**
 * Maps CCK integer, float, and numeric fields to Google Base attributes.
 */
class googlebase_handler_field_numeric extends googlebase_handler_field {

  /**
   * Specifies which Google Base attribute type will be used for this field.
   *
   * @see http://code.google.com/apis/base/docs/2.0/reference.html#AttributesFeed
   *
   * @return string
   *   Possible values are: 'text', 'int', 'float', 'number', 'intUnit',
   *   'floatUnit', 'numberUnit', 'boolean', 'date', 'dateTime', 'dateTimeRange',
   *   'location', 'url', 'reference'.
   */
  public function get_type() {
    switch ($this->field_definition) {
      case 'number_integer':
        return empty($field['widget']['suffix']) ? 'int' : 'intUnit';
      case 'number_decimal':
        return empty($field['widget']['suffix']) ? 'number' : 'numberUnit';
      case 'number_float':
        return empty($field['widget']['suffix']) ? 'float' : 'floatUnit';
    }
  }

  /**
   * Specifies the values that should be sent to Google Base.
   *
   * @return array
   *   An array of values to be sent to Google Base. The values returned should
   *   be based on the values stored in the field which are available in
   *   $this->field_values.
   */
  public function index_values() {
    $values = array();
    $unit = empty($this->field_definition['widget']['suffix']) ? '' : ' ' . $this->field_definition['widget']['suffix'];
    foreach ($this->field_values as $value) {
      $values[] = $value['value'] . $unit;
    }
    return $values;
  }
  
}
