<?php

/**
 * @file googlebase_handler_field_date.inc
 *
 */

/**
 * Maps CCK text fields to Google Base text attributes.
 */
class googlebase_handler_field_date extends googlebase_handler_field {

/**
 * Specifies which Google Base attribute type will be used for this field.
 *
 * @see http://code.google.com/apis/base/docs/2.0/reference.html#AttributesFeed
 *
 * @return string
 *   Possible values are: 'text', 'int', 'float', 'number', 'intUnit',
 *   'floatUnit', 'numberUnit', 'boolean', 'date', 'dateTime', 'dateTimeRange',
 *   'location', 'url', 'reference'.
 */
  public function get_type() {
    $data = array();

    if (!empty($this->field_definition['todate'])) {
      return 'dateTimeRange';
    }
    elseif (isset($this->field_definition['granularity']['hour']) || isset($this->field_definition['granularity']['minute']) || isset($this->field_definition['granularity']['second'])) {
      return 'dateTime';
    }
    else {
      return 'date';
    }
  }

  public function index_values() {
    $values = array();

    if ($this->type == 'dateTimeRange') {
      foreach ($this->field_values as $value) {
        $date1 = googlebase_date_iso(date_convert($value['value'], DATE_ISO, DATE_UNIX));
        $date2 = googlebase_date_iso(date_convert($value['value2'], DATE_ISO, DATE_UNIX));
        $values[] = $date1 . ' ' . $date2;
      }
    }
    elseif ($this->type == 'dateTime') {
      foreach ($this->field_values as $value) {
        $values[] = googlebase_date_iso(date_convert($value['value'], DATE_ISO, DATE_UNIX));
      }
    }
    else {
      foreach ($this->field_values as $value) {
        $date = explode('T', $value['value']);
        $values[] = $date[0];
      }
    }
    return $values;
  }

  protected function convert_date($date) {
    return substr(googlebase_date_iso(date_convert($date, DATE_ISO, DATE_UNIX)), 0, -1);
  }
  
}
