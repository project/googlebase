<?php

class googlebase_item extends googlebase_xml {
  public $xml = NULL;
  protected $namespaces = array(
    'g' => 'http://base.google.com/ns/1.0',
    'app' => 'http://purl.org/atom/app#',
  );

  protected $base_element = 'entry';

  public $node = NULL;

  public function set_draft($draft = FALSE) {
    $value = empty($draft) ? 'no' : 'yes';
    $control = $this->xml->addChild('app:control', NULL, $this->namespaces['app']);
    $node = $control->addChild('app:draft', $value, $this->namespaces['app']);
  }

  public function no_api_syndication() {
    // <g:no_api_syndication>true<g:no_api_syndication>
    $this->add_namespaced_element('g', 'no_api_syndication', 'true');
  }

  public function insert($node) {
    $this->node = $node;
    $headers = googlebase_headers();
    $items_feed_url = GOOGLEBASE_ITEMS_FEED;
    $method = 'POST';

    if (!empty($this->node->googlebase_id)) {
      // Update the item
      $items_feed_url .= '/' . $this->node->googlebase_id;
      $method = 'PUT';
    }
    
    // Execute request
    $result = drupal_http_request($items_feed_url, $headers, $method, $this->get_xml());
    watchdog('googlebase', t('Response from Google Base: ') . print_r($result, TRUE));

    if ($result->code < 0) {
      drupal_set_message(t('There was an error communicating with Google Base: ') . $result->error);
    }
    elseif (($result->code == '200' || $result->code == '201') && !empty($result->headers['Location'])) {
      // Item was successfully added or updated
      // Return ID assigned by Google Base
      return substr(strrchr($result->headers['Location'], '/'), 1);
    }
    elseif ($result->code == '404') {
      if (!empty($this->node->googlebase_id)) {
        // Item used to exist in Google Base, but is no longer found.
        // Re-insert the item.
        googlebase_delete($this->node->nid);
        unset($this->node->googlebase_id);
        return $this->insert();
      }
    }
    if (user_access('administer google base')) {
      try {
        $xml = new SimpleXMLElement($result->data);
        foreach ($xml->error as $error) {
          $field = (string) $error['field'];
          if (!empty($field)) {
            drupal_set_message(t('Error on Google Base field %field: @reason', array('%field' => $field, '@reason' => (string) $error['reason'])), 'error');
          }
          else {
            drupal_set_message(t('Google Base error: @reason', array('@reason' => (string) $error['reason'])), 'error');
          }
        }
      } catch (Exception $e) {
        drupal_set_message(t('There was an error communicating with Google Base.'));
        drupal_set_message(t('Caught exception: ') . $e->getMessage());
      }

    }

    return FALSE;
  }
}