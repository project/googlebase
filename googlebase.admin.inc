<?php

/**
 * FAPI definition for the Google Base configuration form.
 *
 * @ingroup forms
 */
function googlebase_admin_settings_form() {

  $form = array();
  $form['authentication'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $token = googlebase_token();

  // Check if token is still valid.
  if (googlebase_check_token($token)) {
    $status = theme('image', 'misc/watchdog-ok.png', t('Authenticated'), t('Authenticated')) . ' ' . t("You have successfully signed in to your Google Account.");
  }
  else {
    $next_url = url('googlebase/exchange', array('absolute' => TRUE));
    $redirect_url = 'https://www.google.com/accounts/AuthSubRequest?session=1';
    $redirect_url .= '&next=';
    $redirect_url .= urlencode($next_url);
    $redirect_url .= "&scope=";
    $redirect_url .= urlencode(GOOGLEBASE_ITEMS_FEED);

    $status = theme('image', 'misc/watchdog-error.png', t('Error'), t('Error')) . ' ' . t('You must') . ' ' . l('sign in to your Google account', $redirect_url) . '.';
  }
  $form['authentication']['status'] = array(
    '#value' => $status,
  );

  $form['authentication']['googlebase_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Developer Key'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => variable_get('googlebase_key', ''),
  );

  $form = system_settings_form($form);
  return $form;
}

function googlebase_admin_import() {
  $form = array();
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Content Type'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  foreach (googlebase_item_types() as $name => $type) {
    $options[$name] = $type['title'];
  }
  $form['import']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => array('' => t('- select -')) + $options,
    '#description' => t('Select a content type to import.'),
    '#ahah' => array(
      'path' => 'googlebase/ahah/types',
      'wrapper' => 'type-info-wrapper',
      'method' => 'replace',
    ),
    '#suffix' => '<div id="type-info-wrapper"></div>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );
  
  return $form;
}

function googlebase_admin_import_submit($form, &$form_state) {
  include_once('./'. drupal_get_path('module', 'content') .'/includes/content.admin.inc');
  include_once('./'. drupal_get_path('module', 'node') .'/content_types.inc');
  $types = googlebase_item_types();
  $type = $types[$form_state['values']['type']];
  $content = array();

  $content['type'] = array (
    'name' => ucfirst($type['title']),
    'type' => str_replace(" ", "_", $form_state['values']['type']),
    'description' => '',
    'title_label' => 'Title',
    'body_label' => 'Body',
    'min_word_count' => '0',
    'help' => '',
    'node_options' =>
    array (
      'status' => true,
      'promote' => true,
      'sticky' => false,
      'revision' => false,
    ),
    'module' => 'node',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'googlebase' =>
    array (
      'index' => 0,
    ),
  );

  module_load_include('inc', 'content', 'includes/content.crud');

  // Get all type and field info for this database.
  $content_info = _content_type_info();

  $imported_type = $content['type'];
  $imported_type_name = $imported_type['type'];
  $imported_type_label = $imported_type['name'];

  $type = (object) $imported_type;
  $values = $imported_type;
  // Prevent a warning in node/content_types.inc
  $type->has_title = TRUE;
  $type_form_state = array('values' => $values);

  // There's no API for creating node types, we still have to use drupal_execute().
  module_load_include('inc', 'node', 'includes/content_types');
  drupal_execute('node_type_form', $type_form_state, $type);

  // Reset type and database values once new type has been added.
  $type_name  = $imported_type_name;
  $type_label = node_get_types('name', $type_name);
  content_clear_type_cache();
  $content_info = _content_type_info();

  if (form_get_errors() || !isset($content_info['content types']) || !is_array($content_info['content types'][$type_name])) {
     drupal_set_message(t('An error has occurred adding the content type %type.<br/>Please check the errors displayed for more details.', array(
          '%type' => $imported_type_name
          )));
     return;
  }
}

function googlebase_ahah_type_info() {
  $types = googlebase_item_types();
  $type = empty($_REQUEST['googlebase']['type']) ? $types[$_REQUEST['type']] : $types[$_REQUEST['googlebase']['type']];

  $header = array('Field', 'Type');
  $rows = array();
  
  foreach ($type['attributes'] as $attribute) {
    $rows[] = array($attribute['name'], $attribute['type']);
  }

  $output .= theme('status_messages') . theme('table', $header, $rows);
  
  drupal_json(array('status' => TRUE, 'data' => $output));
}